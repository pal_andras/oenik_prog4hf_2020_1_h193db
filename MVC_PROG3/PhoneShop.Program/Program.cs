﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using PhoneShop.Data;
    using PhoneShop.Logic;

    /// <summary>
    /// Fo program.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Main resz.
        /// </summary>
        /// <param name="args">Args.</param>
        private static void Main(string[] args)
        {
            PhoneLogic phoneLogic = new PhoneLogic();
            WorkLogic workLogic = new WorkLogic();
            CustomerLogic customerLogic = new CustomerLogic();

            Console.WriteLine("Isten hozott a Phone Shop-ban. Mit szeretnel csinalni?");
            Console.WriteLine();
            Console.WriteLine("Press 1: Munkafolyamat adatainak listazasa");
            Console.WriteLine("Press 2: Telefon adatainak listazasa");
            Console.WriteLine("Press 3: Vasarlo adatainak listazasa");
            Console.WriteLine("Press 4: Telefon hozzaadasa");
            Console.WriteLine("Press 5: Munka hozzaadasa");
            Console.WriteLine("Press 6: Vasarlo hozzaadasa");
            Console.WriteLine("Press 7: Telefonok listazasa");
            Console.WriteLine("Press 8: Munkak listazasa");
            Console.WriteLine("Press 9: Vasarlok listazasa");
            Console.WriteLine("Press 10: Telefon torlese");
            Console.WriteLine("Press 11: Munka torlese");
            Console.WriteLine("Press 12: Vasarlo torlese");
            Console.WriteLine("Press 13: Telefon aranak modositasa");
            Console.WriteLine("Press 14: Munka aranak modositasa");
            Console.WriteLine("Press 15: Vasarlo email-cimenek modositasa");
            Console.WriteLine("Press 16: Markankent a telefonok atlagos alapara");
            Console.WriteLine("Press 17: Vasarlo megbizasana");
            Console.WriteLine("Press 18 Elerheto szolgaltatasok:");
            Console.WriteLine("press 19: Husegpontok novelese megbizas alapjan");
            Console.WriteLine("Press 20: Torzsvasarlok kilistazasa");
            Console.WriteLine("Press 21: Legdragabb telefon");
            Console.WriteLine("Press 22: Olcso telefon ajanlasa (java vegpont elerese)");
            Console.WriteLine("Press 23: Kilepes");
            Console.Write("Your choice is: ");

            int instrukcio = int.Parse(Console.ReadLine());
            switch (instrukcio)
            {
                case 1:
                    Console.Clear();
                    Console.Write("Adja meg a lekerni szant munka Id-jet: ");
                    int input1 = int.Parse(Console.ReadLine());
                    Console.WriteLine($"Id: {workLogic.GetOne(input1).work_id} \n" +
                        $"Work Name: {workLogic.GetOne(input1).work_name} \n" +
                        $"Price: {workLogic.GetOne(input1).work_price} Ft\n" +
                        $"Time: {workLogic.GetOne(input1).work_time} minutes \n" +
                        $"Parts required: {workLogic.GetOne(input1).work_parts} \n" +
                        $"Worker: {workLogic.GetOne(input1).work_worker} \n");

                    break;
                case 2:
                    Console.Clear();
                    Console.Write("Adja meg a lekerni szant telefon Id-jet: ");
                    int input2 = int.Parse(Console.ReadLine());
                    Console.WriteLine($"Id: {phoneLogic.GetOne(input2).phone_id} \n" +
                        $"Brand: {phoneLogic.GetOne(input2).phone_brand} \n" +
                        $"Type: {phoneLogic.GetOne(input2).phone_type} \n" +
                        $"Price: {phoneLogic.GetOne(input2).phone_price} \n" +
                        $"Screen seize: {phoneLogic.GetOne(input2).phone_inch} inch");
                    if (phoneLogic.GetOne(input2).phone_instock == 1)
                    {
                        Console.WriteLine("Available: Yes");
                    }
                    else
                    {
                        Console.WriteLine("Available: No");
                    }

                    break;
                case 3:
                    Console.Clear();
                    Console.Write("Adja meg a lekerni szant vasarlo Id-jet: ");
                    int input3 = int.Parse(Console.ReadLine());
                    Console.WriteLine($"Id: {customerLogic.GetOne(input3).customer_id} \n" +
                        $"Name: {customerLogic.GetOne(input3).customer_name} \n" +
                        $"Email: {customerLogic.GetOne(input3).customer_email}\n" +
                        $"Pone Number: {customerLogic.GetOne(input3).customer_phonenumber}\n" +
                        $"Date: {customerLogic.GetOne(input3).customer_date} \n" +
                        $"Points: {customerLogic.GetOne(input3).customer_points} \n" +
                        $"Work id: {customerLogic.GetOne(input3).customer_work} \n");

                    break;
                case 4:
                    Console.Clear();
                    Console.WriteLine("Add meg a kovetkezo adatokat!");
                    Console.Write("Telefon Id: ");
                    int pId = int.Parse(Console.ReadLine());
                    Console.Write("Telefon markaja (Iphone, Samsung, Huawei): ");
                    string pbrand = Console.ReadLine();
                    Console.Write("Telefon tipusa: ");
                    string ptype = Console.ReadLine();
                    Console.Write("Telefon ara: ");
                    int pprice = int.Parse(Console.ReadLine());
                    Console.Write("Telefon kepernyojenek merete inchben (0,0): ");
                    float pinch = float.Parse(Console.ReadLine());
                    Console.Write("Telefon van e keszleten (nincs = 0| Van = 1): ");
                    int pinstock = int.Parse(Console.ReadLine());

                    phoneLogic.CreateItem(new PHONE
                    {
                        phone_id = pId,
                        phone_brand = pbrand,
                        phone_type = ptype,
                        phone_price = pprice,
                        phone_inch = pinch,
                        phone_instock = pinstock,
                    });
                    break;
                case 5:
                    Console.Clear();
                    Console.WriteLine("Add meg a kovetkezo adatokat!");
                    Console.Write("Munka Id: ");
                    int wId = int.Parse(Console.ReadLine());
                    Console.Write("Munka megnevezese (Cleaning, Camera fix, Screen replacement, Battery change, Software fix): ");
                    string wname = Console.ReadLine();
                    Console.Write("Munkat vegzo dolgozo neve: ");
                    string wworker = Console.ReadLine();
                    Console.Write("Munka elvegzesenek ara: ");
                    int wprice = int.Parse(Console.ReadLine());
                    Console.Write("Munka elvegzesehez szukseges e valamilyen alkatresz (Yes|No): ");
                    string wparts = Console.ReadLine();
                    Console.Write("Munka elvegzesenek az ideje (30): ");
                    int wtime = int.Parse(Console.ReadLine());
                    Console.Write("Szervezelendo telefon id-je: ");
                    int wphone = int.Parse(Console.ReadLine());

                    workLogic.CreateItem(new WORK
                    {
                        work_id = wId,
                        work_name = wname,
                        work_price = wprice,
                        work_time = wtime,
                        work_parts = wparts,
                        work_phone = wphone,
                        work_worker = wworker,
                    });
                    break;
                case 6:
                    Console.Clear();
                    Console.WriteLine("Add meg a kovetkezo adatokat!");
                    Console.Write("Vasarlo Id: ");
                    int cId = int.Parse(Console.ReadLine());
                    Console.Write("Vasarlo neve: ");
                    string cname = Console.ReadLine();
                    Console.Write("Vasarlo email-cime: ");
                    string cemail = Console.ReadLine();
                    Console.Write("Vasarlo telefonszama: ");
                    string cnumber = Console.ReadLine();
                    Console.Write("Vasarlas datuma (2020.01.01.): ");
                    string cdate = Console.ReadLine();
                    Console.Write("Vasarlo husegpontjainak szama (100): ");
                    int cpoints = int.Parse(Console.ReadLine());
                    Console.Write("Vasarlo altal igenyelt munka id-je: ");
                    int cwork = int.Parse(Console.ReadLine());

                    customerLogic.CreateItem(new CUSTOMER
                    {
                        customer_id = cId,
                        customer_name = cname,
                        customer_email = cemail,
                        customer_phonenumber = cnumber,
                        customer_date = cdate,
                        customer_points = cpoints,
                        customer_work = cwork,
                    });
                    break;
                case 7:
                    Console.Clear();
                    foreach (var item in phoneLogic.Getall())
                    {
                        Console.WriteLine($"{item.phone_id} \t {item.phone_brand} \t {item.phone_type} \t" +
                            $"\t {item.phone_price} \t {item.phone_inch} \t {item.phone_instock}");
                    }

                    break;
                case 8:
                    Console.Clear();
                    foreach (var item in workLogic.Getall())
                    {
                        Console.WriteLine($"{item.work_id} \t {item.work_name} \t {item.work_price} \t" +
                            $"\t {item.work_time} \t {item.work_parts} \t {item.work_phone} \t {item.work_worker}");
                    }

                    break;
                case 9:
                    Console.Clear();
                    foreach (var item in customerLogic.Getall())
                    {
                        Console.WriteLine($"{item.customer_id} \t {item.customer_name} \t {item.customer_email} \t" +
                            $"\t {item.customer_phonenumber} \t {item.customer_date} \t {item.customer_work} \t {item.customer_points}");
                    }

                    break;
                case 10:
                    Console.Clear();
                    Console.Write("Torlendo telefon id-ja: ");
                    int dp = int.Parse(Console.ReadLine());

                    // phoneLogic.DeleteItem(dp);
                    Console.WriteLine($"Sikeresen torolte a {dp} sorszamu telefont.");
                    break;
                case 11:
                    Console.Clear();
                    Console.Write("Torlendo munka folyamat id-ja: ");
                    int dw = int.Parse(Console.ReadLine());

                    // workLogic.DeleteItem(dw);
                    Console.WriteLine($"Sikeresen torolte a {dw} sorszamu munkat.");
                    break;
                case 12:
                    Console.Clear();
                    Console.Write("Torlendo vasarlo id-ja: ");
                    int dc = int.Parse(Console.ReadLine());
                    customerLogic.DeleteItem(dc);
                    Console.WriteLine($"Sikeresen torolte a {dc} sorszamu vasarlot.");
                    break;
                case 13:
                    Console.Clear();
                    Console.Write("Modositando telefon id-ja:");
                    int ujtelefonid = int.Parse(Console.ReadLine());
                    Console.Write("Telefon uj ara: ");
                    int ujtelefonar = int.Parse(Console.ReadLine());
                    phoneLogic.UpdatePrice(ujtelefonid, ujtelefonar);
                    Console.WriteLine("Modositott telefon adatai: ");
                    Console.WriteLine($"Id: {phoneLogic.GetOne(ujtelefonid).phone_id} \n" +
                        $"Brand: {phoneLogic.GetOne(ujtelefonid).phone_brand} \n" +
                        $"Type: {phoneLogic.GetOne(ujtelefonid).phone_type} \n" +
                        $"Price: {phoneLogic.GetOne(ujtelefonid).phone_price} \n" +
                        $"Screen seize: {phoneLogic.GetOne(ujtelefonid).phone_inch} inch");
                    if (phoneLogic.GetOne(ujtelefonid).phone_instock == 1)
                    {
                        Console.WriteLine("Available: Yes");
                    }
                    else
                    {
                        Console.WriteLine("Available: No");
                    }

                    break;
                case 14:
                    Console.Clear();
                    Console.Write("Modositando munka id-ja:");
                    int ujmunkaid = int.Parse(Console.ReadLine());
                    Console.Write("Munka uj ara: ");
                    int ujmunkaar = int.Parse(Console.ReadLine());
                    workLogic.UpdatePrice(ujmunkaid, ujmunkaar);
                    Console.WriteLine("Modositott munka adatai: ");
                    Console.WriteLine($"Id: {workLogic.GetOne(ujmunkaid).work_id} \n" +
                        $"Work Name: {workLogic.GetOne(ujmunkaid).work_name} \n" +
                        $"Price: {workLogic.GetOne(ujmunkaid).work_price} Ft\n" +
                        $"Time: {workLogic.GetOne(ujmunkaid).work_time} minutes \n" +
                        $"Parts required: {workLogic.GetOne(ujmunkaid).work_parts} \n" +
                        $"Worker: {workLogic.GetOne(ujmunkaid).work_worker} \n");
                    break;
                case 15:
                    Console.Clear();
                    Console.Write("Modositando vasarlo id-ja:");
                    int ujvasarloid = int.Parse(Console.ReadLine());
                    Console.Write("Vasarlo uj email-cime: ");
                    string ujemail = Console.ReadLine();
                    customerLogic.UpdateEmail(ujvasarloid, ujemail);
                    Console.WriteLine("Modositott vasarlo adatai: ");
                    Console.WriteLine($"Id: {customerLogic.GetOne(ujvasarloid).customer_id} \n" +
                        $"Name: {customerLogic.GetOne(ujvasarloid).customer_name} \n" +
                        $"Email: {customerLogic.GetOne(ujvasarloid).customer_email}\n" +
                        $"Pone Number: {customerLogic.GetOne(ujvasarloid).customer_phonenumber}\n" +
                        $"Date: {customerLogic.GetOne(ujvasarloid).customer_date} \n" +
                        $"Points: {customerLogic.GetOne(ujvasarloid).customer_points} \n" +
                        $"Work id: {customerLogic.GetOne(ujvasarloid).customer_work} \n");
                    break;
                case 16:
                    Console.Clear();
                    Console.WriteLine("Telefon markankenti atlagos alapara: ");
                    Console.WriteLine($"Iphone: {phoneLogic.AverageBrandCost()[1]} \n" +
                        $"Samsung: {phoneLogic.AverageBrandCost()[2]} \n" +
                        $"Huawei: {phoneLogic.AverageBrandCost()[0]} \n");
                    break;
                case 17:
                    Console.Clear();
                    Console.Write("A vasarlo id-ja: ");
                    int megbizoid = int.Parse(Console.ReadLine());
                    foreach (var item in workLogic.GetTheGivenJobb(customerLogic.GetOne(megbizoid)))
                    {
                        Console.WriteLine("A vasarlo megbizasa: " + item.work_name);
                    }

                    break;
                case 18:
                    Console.Clear();
                    Console.WriteLine("Elerheto szolgaltatasaink: ");
                    foreach (var item in workLogic.ListAllSevices())
                    {
                        Console.WriteLine(item);
                    }

                    break;
                case 19:
                    Console.Clear();
                    Console.Write("Vasarlo azonositoja: ");
                    int novelendoid = int.Parse(Console.ReadLine());
                    customerLogic.GetOne(novelendoid).customer_points = workLogic.IncreaseCustomerpoints(customerLogic.GetOne(novelendoid));
                    Console.WriteLine("A vasarlo uj husegpontjainak szama: " + customerLogic.GetOne(novelendoid).customer_points);
                    break;
                case 20:
                    Console.Clear();
                    Console.WriteLine("Torzsvasarloink nevei: ");
                    foreach (var item in customerLogic.GetAllCustomerWithHighPoints())
                    {
                        Console.WriteLine(item.customer_name);
                    }

                    break;
                case 21:
                    Console.Clear();
                    Console.WriteLine("Legdragabb telefon: ");
                    Console.WriteLine(phoneLogic.GetMostExpensivePhone().phone_brand + " " + phoneLogic.GetMostExpensivePhone().phone_type);
                    break;
                case 22:
                    Console.Clear();
                    Console.Clear();
                    int penz = 200000;
                    int szamlalo = 0;
                    foreach (var item in phoneLogic.Getall())
                    {
                        if (item.phone_price <= penz)
                        {
                            szamlalo++;
                        }
                    }

                    int[] telefonIds = new int[szamlalo];
                    szamlalo = 0;
                    foreach (var item in phoneLogic.Getall())
                    {
                        if (item.phone_price <= penz)
                        {
                            telefonIds[szamlalo] = item.phone_id;
                            szamlalo++;
                        }
                    }

                    string url = "http://localhost:8080/RandomPhone/RandomPhoneServlet?";
                    for (int i = 0; i < telefonIds.Length; i++)
                    {
                        url += $"phoneid{i}=" + telefonIds[i] + "&";
                    }

                    url = url.Remove(url.Length - 1);
                    XDocument xdoc = XDocument.Load(url);
                    Console.WriteLine("Olcso telefon ajanlas: ");
                    int ajanlasid = int.Parse(xdoc.Descendants("Azonosito").First().Value);
                    Console.WriteLine($"Id: {phoneLogic.GetOne(ajanlasid).phone_id} \n" +
                        $"Brand: {phoneLogic.GetOne(ajanlasid).phone_brand} \n" +
                        $"Type: {phoneLogic.GetOne(ajanlasid).phone_type} \n" +
                        $"Price: {phoneLogic.GetOne(ajanlasid).phone_price} \n" +
                        $"Screen seize: {phoneLogic.GetOne(ajanlasid).phone_inch} inch");
                    if (phoneLogic.GetOne(ajanlasid).phone_instock == 1)
                    {
                        Console.WriteLine("Available: Yes");
                    }
                    else
                    {
                        Console.WriteLine("Available: No");
                    }

                    break;
                case 23:
                    Console.Clear();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Rossz inputot adott meg, kerem probalkozzon ujra!");
                    break;
            }

            Console.WriteLine("Program befejezesehez nyomjon meg egy gombot!");
            Console.ReadKey();
        }
    }
}
