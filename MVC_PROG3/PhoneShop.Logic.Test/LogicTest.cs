﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PhoneShop.Data;
    using PhoneShop.Logic;
    using PhoneShop.Repository;

    /// <summary>
    /// Test osztaly.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IPhoneRepository> phoneMockRepo;
        private PhoneLogic phoneController;

        private Mock<IWorkRepository> workMockRepo;
        private WorkLogic workController;

        private Mock<ICustomerRepository> customerMockRepo;
        private CustomerLogic customerController;

        /// <summary>
        /// Mockolt repository.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.phoneMockRepo = new Mock<IPhoneRepository>();
            this.phoneMockRepo.Setup(x => x.GetAll()).Returns(new List<PHONE>()
            {
                new PHONE() { phone_id = 1, phone_brand = "Iphone", phone_type = "11", phone_price = 280000, phone_inch = 6.1, phone_instock = 1 },
                new PHONE() { phone_id = 2, phone_brand = "Samsung", phone_type = "s10", phone_price = 200000, phone_inch = 6.1, phone_instock = 1 },
                new PHONE() { phone_id = 3, phone_brand = "Huawei", phone_type = "p20 pro", phone_price = 130000, phone_inch = 5.8, phone_instock = 1 },
            }.AsQueryable());
            this.phoneController = new PhoneLogic(this.phoneMockRepo.Object);

            this.workMockRepo = new Mock<IWorkRepository>();
            this.workMockRepo.Setup(x => x.GetAll()).Returns(new List<WORK>()
            {
                new WORK() { work_id = 1, work_phone = 2, work_name = "Cleaning", work_price = 5000, work_time = 30, work_parts = "No", work_worker = "Kovács Béla" },
                new WORK() { work_id = 2, work_phone = 1, work_name = "Camera fix", work_price = 20000, work_time = 90, work_parts = "Yes", work_worker = "Nagy János" },
                new WORK() { work_id = 3, work_phone = 1, work_name = "Screen replacement", work_price = 80000, work_time = 15, work_parts = "Yes", work_worker = "Kovács Béla" },
                new WORK() { work_id = 4, work_phone = 2, work_name = "Battery change", work_price = 10000, work_time = 45, work_parts = "Yes", work_worker = "Nagy János" },
                new WORK() { work_id = 5, work_phone = 3, work_name = "Software fix", work_price = 8000, work_time = 60, work_parts = "No", work_worker = "Nagy János" },
            }.AsQueryable());
            this.workController = new WorkLogic(this.workMockRepo.Object);

            this.customerMockRepo = new Mock<ICustomerRepository>();
            this.customerMockRepo.Setup(x => x.GetAll()).Returns(new List<CUSTOMER>()
            {
                new CUSTOMER() { customer_id = 1, customer_work = 1, customer_name = "Pál András", customer_email = "pal.andras55@gmail.com", customer_phonenumber = "06208086841", customer_date = "2020.01.01.", customer_points = 100 },
                new CUSTOMER() { customer_id = 2, customer_work = 2, customer_name = "Kis Ágnes", customer_email = "acadipe-8801@yopmail.com", customer_phonenumber = "06208586841", customer_date = "2019.01.06.", customer_points = 10 },
                new CUSTOMER() { customer_id = 3, customer_work = 3, customer_name = "Nagy Bálint", customer_email = "iffottabe-8893@yopmail.com", customer_phonenumber = "06608086841", customer_date = "2019.01.08.", customer_points = 200 },
                new CUSTOMER() { customer_id = 4, customer_work = 4, customer_name = "Karlik Fanni", customer_email = "xupummova-1275@yopmail.com", customer_phonenumber = "06278086841", customer_date = "2019.01.13.", customer_points = 20 },
                new CUSTOMER() { customer_id = 5, customer_work = 5, customer_name = "Gálcsik Réka", customer_email = "amecedec-7895@yopmail.com", customer_phonenumber = "06208886841", customer_date = "2019.01.24.", customer_points = 50 },
            }.AsQueryable());
            this.customerController = new CustomerLogic(this.customerMockRepo.Object);
        }

        /// <summary>
        /// Egy phone lekerdezesenek a tesztelese.
        /// </summary>
        [Test]
        public void GetOnePhoneTest()
        {
            int id = 3;
            this.phoneController.GetOne(id);
            this.phoneMockRepo.Verify(x => x.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Egy work lekerdezesenek a tesztelese.
        /// </summary>
        [Test]
        public void GetOneWorkTest()
        {
            int id = 3;
            this.workController.GetOne(id);
            this.workMockRepo.Verify(x => x.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Egy customer lekerdezesenek a tesztelese.
        /// </summary>
        [Test]
        public void GetOneCustomerTest()
        {
            int id = 3;
            this.customerController.GetOne(id);
            this.customerMockRepo.Verify(x => x.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Minden phone lekerdezesenek a tesztelese.
        /// </summary>
        [Test]
        public void GetAllPhoneTest()
        {
            IQueryable<PHONE> q = this.phoneController.Getall();
            Assert.IsNotNull(q);
        }

        /// <summary>
        /// Minden work lekerdezesenek a tesztelese.
        /// </summary>
        [Test]
        public void GetAllWorkTest()
        {
            IQueryable<WORK> q = this.workController.Getall();
            Assert.IsNotNull(q);
        }

        /// <summary>
        /// Minden customer lekerdezesenek a tesztelese.
        /// </summary>
        [Test]
        public void GetAllcustomerTest()
        {
            IQueryable<CUSTOMER> q = this.customerController.Getall();
            Assert.IsNotNull(q);
        }

        /// <summary>
        /// Phone torles teszt.
        /// </summary>
        [Test]
        public void DeletePhoneTest()
        {
            int id = 1;
            this.phoneController.DeleteItem(id);
            this.phoneMockRepo.Verify(x => x.DeletePhone(id), Times.Exactly(1));
        }

        /// <summary>
        /// Work torles teszt.
        /// </summary>
        [Test]
        public void DeleteWorkTest()
        {
            int id = 1;
            this.workController.DeleteItem(id);
            this.workMockRepo.Verify(x => x.DeleteWork(id), Times.Exactly(1));
        }

        /// <summary>
        /// Phone torles teszt.
        /// </summary>
        [Test]
        public void DeleteCustomerTest()
        {
            int id = 1;
            this.customerController.DeleteItem(id);
            this.customerMockRepo.Verify(x => x.DeleteCustomer(id), Times.Exactly(1));
        }

        /// <summary>
        /// Phone aranak megvaltoztatasa teszt.
        /// </summary>
        [Test]
        public void UpdatePhonePriceTest()
        {
            int id = 1;
            int newPrice = 100;
            this.phoneController.UpdatePrice(id, newPrice);
            this.phoneMockRepo.Verify(x => x.UpdatePhonePrice(1, 100));
        }

        /// <summary>
        /// Work aranak megvaltoztatasa teszt.
        /// </summary>
        [Test]
        public void UpdateWorkPriceTest()
        {
            int id = 1;
            int newPrice = 100;
            this.workController.UpdatePrice(id, newPrice);
            this.workMockRepo.Verify(x => x.UpdateWorkPrice(1, 100));
        }

        /// <summary>
        /// Customer email-cimenek a megvaltoztatasa teszt.
        /// </summary>
        [Test]
        public void UpdateCustomerEmailTest()
        {
            int id = 1;
            string newemail = "lap.andras65@gmail.com";
            this.customerController.UpdateEmail(id, newemail);
            this.customerMockRepo.Verify(x => x.UpdateCustomerEmail(1, "lap.andras65@gmail.com"));
        }

        /// <summary>
        /// Phone hozzaadas Test.
        /// </summary>
        [Test]
        public void AddPhoneTest()
        {
            PHONE phone = new PHONE()
            {
                phone_id = 5,
                phone_brand = "Iphone",
                phone_type = "11",
                phone_price = 280000,
                phone_inch = 6.1,
                phone_instock = 1,
            };
            this.phoneController.CreateItem(phone);
            this.phoneMockRepo.Verify(v => v.CreatePhone(It.IsAny<PHONE>()), Times.Exactly(1));
        }

        /// <summary>
        /// Work hozzaadas Test.
        /// </summary>
        [Test]
        public void AddWorkTest()
        {
            WORK work = new WORK()
            {
                work_id = 6,
                work_name = "Cleaning",
                work_parts = "No",
                work_phone = 1,
                work_price = 5000,
                work_time = 15,
                work_worker = "Pal Andras",
            };
            this.workController.CreateItem(work);
            this.workMockRepo.Verify(v => v.CreateWork(It.IsAny<WORK>()), Times.Exactly(1));
        }

        /// <summary>
        /// Customer hozzaadas Test.
        /// </summary>
        [Test]
        public void AddCustomerTest()
        {
            CUSTOMER customer = new CUSTOMER()
            {
                customer_id = 6,
                customer_name = "Molnár Aladár",
                customer_date = "2020.01.07.",
                customer_email = "molnar.aladar66@gmail.com",
                customer_phonenumber = "06304355413",
                customer_points = 0,
                customer_work = 2,
            };
            this.customerController.CreateItem(customer);
            this.customerMockRepo.Verify(v => v.CreateCustomer(It.IsAny<CUSTOMER>()), Times.Exactly(1));
        }

        /// <summary>
        /// Legdragabb telefon lekerdezesenek a tesztje.
        /// </summary>
        [Test]
        public void MostExpensivePhoneTest()
        {
            Assert.AreEqual(this.phoneController.GetMostExpensivePhone().phone_price, 280000);
        }

        /// <summary>
        /// Szolgaltatasok lestazasanak tesztelese.
        /// </summary>
        [Test]
        public void ListAllServicestest()
        {
            Assert.IsNotNull(this.workController.ListAllSevices());
        }

        /// <summary>
        /// Atlag marka arak tesztelese.
        /// </summary>
        [Test]
        public void AverageBrandCostTest()
        {
            int[] atlagok = new int[]
            {
                280000,
                200000,
                130000,
            };
            Assert.AreEqual(atlagok[0], this.phoneController.AverageBrandCost()[0]);
            Assert.AreEqual(atlagok[1], this.phoneController.AverageBrandCost()[1]);
            Assert.AreEqual(atlagok[2], this.phoneController.AverageBrandCost()[2]);
        }

        /// <summary>
        /// Megbizas teszt.
        /// </summary>
        [Test]
        public void GetTheGivenJobbTest()
        {
            CUSTOMER customer = new CUSTOMER()
            {
                customer_id = 6,
                customer_name = "Molnár Aladár",
                customer_date = "2020.01.07.",
                customer_email = "molnar.aladar66@gmail.com",
                customer_phonenumber = "06304355413",
                customer_points = 100,
                customer_work = 1,
            };
            var q = from x in this.workController.Getall()
                    where x.work_id == customer.customer_work
                    select x;
            Assert.IsNotNull(q);
        }

        /// <summary>
        /// Husegpontnovelo teszt.
        /// </summary>
        [Test]
        public void IncreaseCustomerpointsTest()
        {
            CUSTOMER customer = new CUSTOMER()
            {
                customer_id = 6,
                customer_name = "Molnár Aladár",
                customer_date = "2020.01.07.",
                customer_email = "molnar.aladar66@gmail.com",
                customer_phonenumber = "06304355413",
                customer_points = 100,
                customer_work = 1,
            };
            int uj = 110;
            Assert.AreEqual(uj, this.workController.IncreaseCustomerpoints(customer));
        }
    }
}
