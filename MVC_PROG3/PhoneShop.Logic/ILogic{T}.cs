﻿// <copyright file="ILogic{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhoneShop.Data;
    using PhoneShop.Repository;

    /// <summary>
    /// Uzleti logika megkoteseit tartalmazo interface.
    /// </summary>
    /// <typeparam name="T">Generikus tipus.</typeparam>
    public interface ILogic<T>
        where T : class
    {
        /// <summary>
        /// Visszaad egy tetelt.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <returns>Egy tetel.</returns>
        T GetOne(int id);

        /// <summary>
        /// Visszaad minden tetelt.
        /// </summary>
        /// <returns>Minden tetel.</returns>
        IQueryable<T> Getall();

        /// <summary>
        /// Letrehoz egy tetelt.
        /// </summary>
        /// <param name="obj">Generikus tipus az adatbazisbol.</param>
        void CreateItem(T obj);

        /// <summary>
        /// Torol egy tetelt.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        void DeleteItem(int id);
    }

    /// <summary>
    /// PhoneLogic interface.
    /// </summary>
    public interface IPhoneLogic : ILogic<PHONE>
    {
        /// <summary>
        /// Modositja egy tetel arat az adatbazisba.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newprice">Tetel uj ara.</param>
        void UpdatePrice(int id, int newprice);
    }

    /// <summary>
    /// WorkLogic interface.
    /// </summary>
    public interface IWorkLogic : ILogic<WORK>
    {
        /// <summary>
        /// Modositja egy tetel arat az adatbazisba.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="neworice">Tetel uj ara.</param>
        void UpdatePrice(int id, int neworice);
    }

    /// <summary>
    /// Icustomer interface.
    /// </summary>
    public interface ICustomerLogic : ILogic<CUSTOMER>
    {
        /// <summary>
        /// Modositja a tetel email.cimet.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newemail">Tetel uj email-cime.</param>
        void UpdateEmail(int id, string newemail);

        /// <summary>
        /// Megvaltoztata egy customer adatait
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="name">name</param>
        /// <param name="email">email</param>
        /// <param name="phonenumber">phonenumber</param>
        /// <param name="workid">workid</param>
        /// <param name="point">point</param>
        /// <param name="date">date</param>
        void EditCustomer(int id, string name, string email, string phonenumber, int workid, int point, string date);
        bool DelCustomer(int id);
        bool ModCustomer(int id, string name, string email, string phonenumber, int workid, int point, string date);
    }

    /// <summary>
    /// PhoneLogic osztaly.
    /// </summary>
    public class PhoneLogic : IPhoneLogic
    {
        /// <summary>
        /// Phone repozitory mezo.
        /// </summary>
        private readonly IPhoneRepository phonerepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneLogic"/> class.
        /// Konstruktorral beallitjuk az adatbazist.
        /// </summary>
        public PhoneLogic()
        {
            this.phonerepo = new PhoneRepository(new PhoneShopDataBaseEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneLogic"/> class.
        /// Konstruktor.
        /// </summary>
        /// <param name="repo">IPhonerepository tipus.</param>
        public PhoneLogic(IPhoneRepository repo)
        {
            this.phonerepo = repo;
        }

        /// <summary>
        /// Letrehozunk egy tetelt az adatbazisban.
        /// </summary>
        /// <param name="obj">Tetel amit hozzaadunk.</param>
        public void CreateItem(PHONE obj)
        {
            this.phonerepo.CreatePhone(obj);
        }

        /// <summary>
        /// Torlunk egy tetelt az adatbazisbol.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        public void DeleteItem(int id)
        {
            this.phonerepo.DeletePhone(id);
        }

        /// <summary>
        /// Visszaad minden tetelt az adatbazisbol.
        /// </summary>
        /// <returns>Minden tetel.</returns>
        public IQueryable<PHONE> Getall()
        {
            return this.phonerepo.GetAll();
        }

        /// <summary>
        /// Visszaad egy tetelt az adatbazisbol.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <returns>Tetel.</returns>
        public PHONE GetOne(int id)
        {
            return this.phonerepo.GetOne(id);
        }

        /// <summary>
        /// Modositja egy tetelnek az arat.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newprice">Tetel uj ara.</param>
        public void UpdatePrice(int id, int newprice)
        {
            this.phonerepo.UpdatePhonePrice(id, newprice);
        }

        /// <summary>
        /// Lekerdezzuk a legdragabb telefont.
        /// </summary>
        /// <returns>Phone obj.</returns>
        public PHONE GetMostExpensivePhone()
        {
            var phone = from x in this.phonerepo.GetAll()
                        orderby x.phone_price descending
                        select x;
            return phone.First();
        }

        /// <summary>
        /// Kiirja a markak atlagos alaparat.
        /// </summary>
        /// <returns>Int tomb.</returns>
        public int[] AverageBrandCost()
        {
            List<string> lista = new List<string>();
            var q = from x in this.phonerepo.GetAll()
                    group x by x.phone_brand into y
                    select y;
            foreach (var item in q)
            {
                lista.Add(item.Key);
            }

            int[] brandosszegek = new int[lista.Count];
            int[] branddarab = new int[lista.Count];
            foreach (var item in this.phonerepo.GetAll())
            {
                if (item.phone_brand == lista[0])
                {
                    brandosszegek[0] += item.phone_price.Value;
                    branddarab[0]++;
                }

                if (item.phone_brand == lista[1])
                {
                    brandosszegek[1] += item.phone_price.Value;
                    branddarab[1]++;
                }

                if (item.phone_brand == lista[2])
                {
                    brandosszegek[2] += item.phone_price.Value;
                    branddarab[2]++;
                }
            }

            int[] atlagok = new int[lista.Count];
            atlagok[0] = brandosszegek[0] / branddarab[0];
            atlagok[1] = brandosszegek[1] / branddarab[1];
            atlagok[2] = brandosszegek[2] / branddarab[2];
            return atlagok;
        }
    }

    /// <summary>
    /// WorkLogic osztaly.
    /// </summary>
    public class WorkLogic : IWorkLogic
    {
        private readonly IWorkRepository workrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkLogic"/> class.
        /// Konstruktorral beallitjuk az adatbazist.
        /// </summary>
        public WorkLogic()
        {
            this.workrepo = new WorkRepository(new PhoneShopDataBaseEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkLogic"/> class.
        /// Konstruktorral beallitjuk az adatbazist.
        /// </summary>
        /// <param name="repo">IWorkRepository tipus.</param>
        public WorkLogic(IWorkRepository repo)
        {
            this.workrepo = repo;
        }

        /// <summary>
        /// Letrehoz egy tetelt az adatbazisba.
        /// </summary>
        /// <param name="obj">Tetel.</param>
        public void CreateItem(WORK obj)
        {
            this.workrepo.CreateWork(obj);
        }

        /// <summary>
        /// Torol egy tetelt az adatbazisbol.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        public void DeleteItem(int id)
        {
            this.workrepo.DeleteWork(id);
        }

        /// <summary>
        /// Viszaad minden tetelt az adatbazisbol.
        /// </summary>
        /// <returns>Minden tetel.</returns>
        public IQueryable<WORK> Getall()
        {
            return this.workrepo.GetAll();
        }

        /// <summary>
        /// Visszaad egy tetelt az datbazisbol.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <returns>Tetel.</returns>
        public WORK GetOne(int id)
        {
            return this.workrepo.GetOne(id);
        }

        /// <summary>
        /// Modositja egy tetel arat.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newprice">Tetel uj ara.</param>
        public void UpdatePrice(int id, int newprice)
        {
            this.workrepo.UpdateWorkPrice(id, newprice);
        }

        /// <summary>
        /// Kilistazza a szolgaltatasainkat.
        /// </summary>
        /// <returns>Lista ami stringeket tartalmaz.</returns>
        public List<string> ListAllSevices()
        {
            List<string> lista = new List<string>();
            var q = from x in this.workrepo.GetAll()
                    group x by x.work_name into y
                    select y;
            foreach (var item in q)
            {
                lista.Add(item.Key);
            }

            return lista;
        }

        /// <summary>
        /// Visszaadja hogy a vasarlo milyen megbizast adott le.
        /// </summary>
        /// <param name="customer">Customer.</param>
        /// <returns>Work.</returns>
        public IQueryable<WORK> GetTheGivenJobb(CUSTOMER customer)
        {
            var q = from x in this.workrepo.GetAll()
                    where x.work_id == customer.customer_work
                    select x;
            return q;
        }

        /// <summary>
        /// Kiirja a megnovelt husegpontjait az altala leadott munk alapjan.
        /// </summary>
        /// <param name="customer">Customer.</param>
        /// <returns>Int.</returns>
        public int IncreaseCustomerpoints(CUSTOMER customer)
        {
            int newPoints = customer.customer_points.Value;
            foreach (var item in this.workrepo.GetAll())
            {
                if (item.work_id == customer.customer_work)
                {
                    if (item.work_price < 10000)
                    {
                        newPoints += 10;
                    }

                    if (item.work_price > 10000 && item.work_price < 20000)
                    {
                        newPoints += 20;
                    }

                    if (item.work_price > 20000 && item.work_price < 50000)
                    {
                        newPoints += 30;
                    }

                    if (item.work_price > 50000)
                    {
                        newPoints += 50;
                    }
                }
            }

            return newPoints;
        }
    }

    /// <summary>
    /// CustomerLogic osztaly.
    /// </summary>
    public class CustomerLogic : ICustomerLogic
    {
        private readonly ICustomerRepository customerrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerLogic"/> class.
        /// Konstruktorral beallitjuk az adatbazist.
        /// </summary>
        public CustomerLogic()
        {
            this.customerrepo = new CustomerRepository(new PhoneShopDataBaseEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerLogic"/> class.
        /// Konstruktorral beallitjuk az adatbazist.
        /// </summary>
        /// <param name="repo">ICustomerRepository tipus.</param>
        public CustomerLogic(ICustomerRepository repo)
        {
            this.customerrepo = repo;
        }

        /// <summary>
        /// Letrehozunk egy tetelt.
        /// </summary>
        /// <param name="obj">Tetel.</param>
        public void CreateItem(CUSTOMER obj)
        {
            this.customerrepo.CreateCustomer(obj);
        }

        /// <summary>
        /// Torlunk egy tetelt az adatbazisbol.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        public void DeleteItem(int id)
        {
            this.customerrepo.DeleteCustomer(id);
        }

        /// <summary>
        /// Visszaad minden tetelt az adatbazisbol.
        /// </summary>
        /// <returns>Minden tetel.</returns>
        public IQueryable<CUSTOMER> Getall()
        {
            return this.customerrepo.GetAll();
        }

        /// <summary>
        /// Visszaad egy tetelt az adatbazisbol.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <returns>Tetel.</returns>
        public CUSTOMER GetOne(int id)
        {
            return this.customerrepo.GetOne(id);
        }

        /// <summary>
        /// Modositja a tetel email-cimet.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newemail">Tetel uj emil-cime.</param>
        public void UpdateEmail(int id, string newemail)
        {
            this.customerrepo.UpdateCustomerEmail(id, newemail);
        }

        /// <summary>
        /// Visszaadja a magas husegpontal rendelkezo ugyfeleket.
        /// </summary>
        /// <returns>Osszes magas pontszamu customer.</returns>
        public IQueryable<CUSTOMER> GetAllCustomerWithHighPoints()
        {
            var q = from x in this.customerrepo.GetAll()
                    where x.customer_points > 150
                    select x;
            return q;
        }

        /// <summary>
        /// Megvaltoztata egy customer adatait
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="name">name</param>
        /// <param name="email">email</param>
        /// <param name="phonenumber">phonenumber</param>
        /// <param name="workid">workid</param>
        /// <param name="point">point</param>
        /// <param name="date">date</param>
        public void EditCustomer(int id, string name, string email, string phonenumber, int workid, int point, string date)
        {
            this.customerrepo.EditCustomer(id, name, email, phonenumber, workid, point, date);
        }
        public bool DelCustomer(int id)
        {
            return customerrepo.DelCustomer(id);
        }
        public bool ModCustomer(int id, string name, string email, string phonenumber, int workid, int point, string date)
        {
            return customerrepo.ModCustomer(id, name, email, phonenumber, workid, point, date);
        }
    }
}
