﻿// <copyright file="IRepository{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhoneShop.Data;

    /// <summary>
    /// Interface a Repositoryhoz ami bizonyos szabalyokat ir elo.
    /// </summary>
    /// <typeparam name="T">T tipus ami lehet PHONE,WORK,CUSTOMER.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Visszaad egy T tipusu elemet egy int tipusu id alapjan.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <returns>T tipusu elem.</returns>
        T GetOne(int id);

        /// <summary>
        /// Visszaadja az osszes T tipusu elemet.
        /// </summary>
        /// <returns>Minden T tipusu elem.</returns>
        IQueryable<T> GetAll();
    }

    /// <summary>
    /// Phone repository interface ami ami megvalositja az Irepositoryt a Phone tipusokra nezve.
    /// </summary>
    public interface IPhoneRepository : IRepository<PHONE>
    {
        /// <summary>
        /// Letrehoz egy phone tipusu objektumot.
        /// </summary>
        /// <param name="phone">Phone tipusu objektum.</param>
        void CreatePhone(PHONE phone);

        // PHONE GetPhoneId(int id);

        /// <summary>
        /// Megvaltoztatja egy Phone arat.
        /// </summary>
        /// <param name="id">azonosito.</param>
        /// <param name="newprice">Uj ar.</param>
        void UpdatePhonePrice(int id, int newprice);

        /// <summary>
        /// Torol egy Phone objektumot.
        /// </summary>
        /// <param name="id">azonosito.</param>
        void DeletePhone(int id);
    }

    /// <summary>
    /// Work repository interface ami ami megvalositja az Irepositoryt a Work tipusokra nezve.
    /// </summary>
    public interface IWorkRepository : IRepository<WORK>
    {
        /// <summary>
        /// Letrehoz egy Work tipusu objektumot.
        /// </summary>
        /// <param name="work">Work tipusu objektum.</param>
        void CreateWork(WORK work);

        /// <summary>
        /// Megvaltoztatja egy Work objektum arat.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newprice">Uj ar.</param>
        void UpdateWorkPrice(int id, int newprice);

        /// <summary>
        /// Torol egy Work objektumot.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        void DeleteWork(int id);
    }

    /// <summary>
    /// Customer repository interface ami ami megvalositja az Irepositoryt a Customer tipusokra nezve.
    /// </summary>
    public interface ICustomerRepository : IRepository<CUSTOMER>
    {
        /// <summary>
        /// Letrehoz egy Customer objektumot.
        /// </summary>
        /// <param name="customer">Customer tipusu objektum.</param>
        void CreateCustomer(CUSTOMER customer);

        /// <summary>
        /// Megvaltoztatja egy customer email-cimet.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newemail">Customer uj email cime.</param>
        void UpdateCustomerEmail(int id, string newemail);

        /// <summary>
        /// Torol egy Customer objektumot.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        void DeleteCustomer(int id);

        /// <summary>
        /// Egy customer adatainak a megvaltoztatasa
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="name">name</param>
        /// <param name="email">email</param>
        /// <param name="phonenumber">phonenumber</param>
        /// <param name="workid">workid</param>
        /// <param name="point">point</param>
        /// <param name="date">date</param>
        void EditCustomer(int id, string name, string email, string phonenumber, int workid, int point, string date);
        bool DelCustomer(int id);
        bool ModCustomer(int id, string name, string email, string phonenumber, int workid, int point, string date);
    }

    /// <summary>
    /// PhoneRepository osztaly ami megvalositja az IphoneRepository interfacet.
    /// </summary>
    public class PhoneRepository : IPhoneRepository
    {
        private readonly PhoneShopDataBaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneRepository"/> class.
        /// Konstruktor ami beallitja a PhoneRepositoryt.
        /// </summary>
        /// <param name="db">PhoneShopDataBaseEntities tipus.</param>
        public PhoneRepository(PhoneShopDataBaseEntities db)
        {
            this.db = db;
        }

        /// <summary>
        /// Visszaad egy tetelet az adatbazisbol egy azonosito alapjan.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <returns>Phone tipusu tetel.</returns>
        public PHONE GetOne(int id)
        {
            return this.db.PHONE.Where(x => x.phone_id == id).FirstOrDefault();
        }

        /// <summary>
        /// Visszaad minden tetelt az adatbazisbol.
        /// </summary>
        /// <returns>Minden tetel az adatbazisbol.</returns>
        public IQueryable<PHONE> GetAll()
        {
            return this.db.PHONE;
        }

        /// <summary>
        /// Letrehoz egy Phonet az adatbazisba.
        /// </summary>
        /// <param name="phone">Tetel amit az adatbazisba tesz.</param>
        public void CreatePhone(PHONE phone)
        {
            this.db.PHONE.Add(phone);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Torol egy phone tipusu tetelt az adatbazisbol.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        public void DeletePhone(int id)
        {
            this.db.PHONE.Remove(this.GetOne(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Modositja a tetel arat az adatbazisban.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newprice">A tetel uj ara.</param>
        public void UpdatePhonePrice(int id, int newprice)
        {
            var phone = this.GetOne(id);
            phone.phone_price = newprice;
            this.db.SaveChanges();
        }
    }

    /// <summary>
    /// Workrepository osztaly ami megvalositja az Iworkrepository interfacet.
    /// </summary>
    public class WorkRepository : IWorkRepository
    {
        private readonly PhoneShopDataBaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRepository"/> class.
        /// Konstruktor ami beallitja a Workrepositoryt.
        /// </summary>
        /// <param name="db">PhoneShopDataBaseEntities tipusu adatbazis.</param>
        public WorkRepository(PhoneShopDataBaseEntities db)
        {
            this.db = db;
        }

        /// <summary>
        /// Letrehoz egy Work tetelt az adatbazisba.
        /// </summary>
        /// <param name="work">Work tipusu tetel.</param>
        public void CreateWork(WORK work)
        {
            this.db.WORK.Add(work);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Torol egy azonosito alapjan egy work tipusu tetelt az adatbazisbol.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        public void DeleteWork(int id)
        {
            this.db.WORK.Remove(this.GetOne(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Visszaad minden tetelt az adatbazisbol.
        /// </summary>
        /// <returns>Minden tetel.</returns>
        public IQueryable<WORK> GetAll()
        {
            return this.db.WORK;
        }

        /// <summary>
        /// Visszaad egy tetelt az adatbazisbol egy azonosito alapjan.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <returns>Work tipusu tetel.</returns>
        public WORK GetOne(int id)
        {
            return this.db.WORK.Where(x => x.work_id == id).FirstOrDefault();
        }

        /// <summary>
        /// Modosit egy tetelnek az arat.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newprice">Tetel uj ara.</param>
        public void UpdateWorkPrice(int id, int newprice)
        {
            var work = this.GetOne(id);
            work.work_price = newprice;
            this.db.SaveChanges();
        }
    }

    /// <summary>
    /// Customerrepository osztaly ami megvalositja az ICustomerrepository interfacet.
    /// </summary>
    public class CustomerRepository : ICustomerRepository
    {
        private readonly PhoneShopDataBaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// konstruktor ami beallitja Customerrepositoryt.
        /// </summary>
        /// <param name="db">PhoneShopDataBaseEntities tipusu adatbazis.</param>
        public CustomerRepository(PhoneShopDataBaseEntities db)
        {
            this.db = db;
        }

        /// <summary>
        /// Letrehoz egy customer tipusu tetelt az adatbazisba.
        /// </summary>
        /// <param name="customer">Customer tipusu tetel.</param>
        public void CreateCustomer(CUSTOMER customer)
        {
            this.db.CUSTOMER.Add(customer);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Torol egy azonosito alapjan egy tetelt az adatbazisbol.
        /// </summary>
        /// <param name="id">azonosito.</param>
        public void DeleteCustomer(int id)
        {
            this.db.CUSTOMER.Remove(this.GetOne(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Visszaad minden tetelt az adatbazisbol.
        /// </summary>
        /// <returns>Minden tetel.</returns>
        public IQueryable<CUSTOMER> GetAll()
        {
            return this.db.CUSTOMER;
        }

        /// <summary>
        /// Visszaad egy azonosito alapjan egy Cuustomer tipusu tetelt.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <returns>Customer tipusu tetel.</returns>
        public CUSTOMER GetOne(int id)
        {
            return this.db.CUSTOMER.Where(x => x.customer_id == id).FirstOrDefault();
        }

        /// <summary>
        /// Modositja az egyik tetelek az email-cimet az adatbazisba.
        /// </summary>
        /// <param name="id">Azonosito.</param>
        /// <param name="newemail">Tetel uj email-cime.</param>
        public void UpdateCustomerEmail(int id, string newemail)
        {
            var customer = this.GetOne(id);
            customer.customer_email = newemail;
            this.db.SaveChanges();
        }

        /// <summary>
        /// Megvaltoztata egy customer adatait
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="name">name</param>
        /// <param name="email">email</param>
        /// <param name="phonenumber">phonenumber</param>
        /// <param name="workid">workid</param>
        /// <param name="point">point</param>
        /// <param name="date">date</param>
        public void EditCustomer(int id, string name, string email, string phonenumber, int workid, int point, string date)
        {
            var customer = GetOne(id);
            customer.customer_date = date;
            customer.customer_email = email;
            customer.customer_name = name;
            customer.customer_phonenumber = phonenumber;
            customer.customer_points = point;
            customer.customer_work = workid;
            this.db.SaveChanges();
        }
        public bool ModCustomer(int id, string name, string email, string phonenumber, int workid, int point, string date)
        {
            var customer = GetOne(id);
            if (customer == null)
            {
                return false;
            }
            else
            {
                customer.customer_date = date;
                customer.customer_email = email;
                customer.customer_name = name;
                customer.customer_phonenumber = phonenumber;
                customer.customer_points = point;
                customer.customer_work = workid;
                this.db.SaveChanges();
                return true;
            }
        }
        public bool DelCustomer(int id)
        {
            if (this.GetOne(id) == null)
            {
                return false;
            }
            else
            {
                this.db.CUSTOMER.Remove(this.GetOne(id));
                this.db.SaveChanges();
                return true;
            }
        }
    }
}
