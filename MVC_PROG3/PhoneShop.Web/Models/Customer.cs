﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoneShop.Web.Models
{
	public class Customer
	{
		[Display(Name = "Customer Id")]
		[Required]
		public int Id { get; set; }

		[Display(Name = "Work Id")]
		[Required]
		public int WorkId { get; set; }

		[Display(Name = "Customer name")]
		[Required]
		public string Name { get; set; }

		[Display(Name = "Customer email")]
		[Required]
		public string Email { get; set; }

		[Display(Name = "Customer phone number")]
		[Required]
		public string PhoneNumber { get; set; }

		[Display(Name = "Customer date")]
		[Required]
		public string Date { get; set; }

		[Display(Name = "Customer points")]
		[Required]
		public int CustomerPoints { get; set; }
	}
}