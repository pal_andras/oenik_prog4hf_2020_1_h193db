﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace PhoneShop.Web.Models
{
	public class MapperFactory
	{
		public static IMapper CreateMapper()
		{
			var config = new MapperConfiguration(cfg =>
			{
				cfg.CreateMap<PhoneShop.Data.CUSTOMER, PhoneShop.Web.Models.Customer>().
				ForMember(dest => dest.Id, map => map.MapFrom(scr => scr.customer_id)).
				ForMember(dest => dest.Name, map => map.MapFrom(scr => scr.customer_name)).
				ForMember(dest => dest.PhoneNumber, map => map.MapFrom(scr => scr.customer_phonenumber)).
				ForMember(dest => dest.Email, map => map.MapFrom(scr => scr.customer_email)).
				ForMember(dest => dest.Date, map => map.MapFrom(scr => scr.customer_date)).
				ForMember(dest => dest.WorkId, map => map.MapFrom(scr => scr.customer_work)).
				ForMember(dest => dest.CustomerPoints, map => map.MapFrom(scr => scr.customer_points));
			});
			return config.CreateMapper();
		}
	}
}