﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PhoneShop.Data;
using PhoneShop.Logic;
using PhoneShop.Repository;
using PhoneShop.Web.Models;

namespace PhoneShop.Web.Controllers
{
    public class CustomerController : Controller
    {
        ICustomerLogic logic;
        IMapper mapper;
        CustomerViewModel vm;
        public CustomerController()
        {
            PhoneShopDataBaseEntities db = new PhoneShopDataBaseEntities();
            CustomerRepository customRepo = new CustomerRepository(db);
            logic = new CustomerLogic(customRepo);
            mapper = MapperFactory.CreateMapper();
            vm = new CustomerViewModel();
            vm.EditCustomer = new Customer();
            var customers = logic.Getall().ToList();
            vm.ListOfCustomres = mapper.Map<IList<Data.CUSTOMER>, List<Models.Customer>>(customers);

        }
        private Customer GetCustomerModel(int id)
        {
            CUSTOMER oneCustomer = logic.GetOne(id);
            return mapper.Map<Data.CUSTOMER, Models.Customer>(oneCustomer);
        }
        // GET: Customer
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("CustomerIndex", vm);
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View("CustomerDetails", GetCustomerModel(id));
        }

        // GET: Customer/Remove/5
        public ActionResult Remove(int id)
        {
            //TempData["editResult"] = "Delete FAIL";
            //if (logic.DeleteItem(id))
            //{

            //}
            logic.DeleteItem(id);
            TempData["editResult"] = "Delete OK";
            return RedirectToAction("Index");
        }
        //GET: Customer/edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditCustomer = GetCustomerModel(id);
            return View("CustomerIndex", vm);
        }
        // POST: Customer/Edit
        [HttpPost]
        public ActionResult Edit(Customer customer, string editAction)
        {
            CUSTOMER c = new CUSTOMER();
            c.customer_id = customer.Id;
            c.customer_name = customer.Name;
            c.customer_date = customer.Date;
            c.customer_email = customer.Email;
            c.customer_phonenumber = customer.PhoneNumber;
            c.customer_points = customer.CustomerPoints;
            c.customer_work = customer.WorkId;
            if (ModelState.IsValid && customer != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.CreateItem(c);
                }
                else
                {
                    logic.EditCustomer(customer.Id, customer.Name, customer.Email, customer.PhoneNumber, customer.WorkId, customer.CustomerPoints, customer.Date);
                    //logic.UpdateEmail(customer.Id, "UpdatedEmail");
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditCustomer = customer;
                return View("CustomerIndex", vm);
            }
        }


        //// GET: Customer/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Customer/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Customer/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: Customer/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Customer/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Customer/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
