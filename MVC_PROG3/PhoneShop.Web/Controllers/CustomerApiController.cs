﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using PhoneShop.Data;
using PhoneShop.Logic;
using PhoneShop.Repository;
using PhoneShop.Web.Models;

namespace PhoneShop.Web.Controllers
{
	public class CustomerApiController : ApiController
    {
		public class ApiResult
		{
			public bool OperationResult { get; set; }
		}
		ICustomerLogic logic;
		IMapper mapper;
		public CustomerApiController()
		{
			PhoneShopDataBaseEntities db = new PhoneShopDataBaseEntities();
			CustomerRepository customRepo = new CustomerRepository(db);
			logic = new CustomerLogic(customRepo);
			mapper = MapperFactory.CreateMapper();
		}
		//GET: /api/CustomersApi  --alap
		[ActionName("all")]
		[HttpGet]
		//GET: api/CustomerApi/all
		public IEnumerable<Models.Customer> GetAll()
		{
			var customers = logic.Getall();
			return mapper.Map<IQueryable<Data.CUSTOMER>, List<Models.Customer>>(customers);
		}
		// GET: api/CustomerApi/del/42
		[ActionName("del")]
		[HttpGet]
		public ApiResult DelOneCustomer(int id)
		{
			bool success = logic.DelCustomer(id);
			return new ApiResult() { OperationResult = success };
		}
		//POST: api/CustomerApi/add + customer
		[ActionName("add")]
		[HttpPost]
		public ApiResult AddOneCustomer(Customer customer)
		{
			CUSTOMER c = new CUSTOMER();
			c.customer_id = customer.Id;
			c.customer_name = customer.Name;
			c.customer_date = customer.Date;
			c.customer_email = customer.Email;
			c.customer_phonenumber = customer.PhoneNumber;
			c.customer_points = customer.CustomerPoints;
			c.customer_work = customer.WorkId;
			logic.CreateItem(c);
			return new ApiResult() { OperationResult = true };
		}
		//POST api/CustomerApi/mod
		[ActionName("mod")]
		[HttpPost]
		public ApiResult ModeOneCustomer(Customer customer)
		{
			bool success = logic.ModCustomer(customer.Id, customer.Name, customer.Email, customer.PhoneNumber, customer.WorkId, customer.CustomerPoints, customer.Date);
			return new ApiResult() { OperationResult = success };
		}
    }
}
