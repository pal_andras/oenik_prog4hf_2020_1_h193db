var hierarchy =
[
    [ "PhoneShop.Data.CUSTOMER", "class_phone_shop_1_1_data_1_1_c_u_s_t_o_m_e_r.html", null ],
    [ "DbContext", null, [
      [ "PhoneShop.Data.PhoneShopDataBaseEntities", "class_phone_shop_1_1_data_1_1_phone_shop_data_base_entities.html", null ]
    ] ],
    [ "PhoneShop.Logic.ILogic< T >", "interface_phone_shop_1_1_logic_1_1_i_logic.html", null ],
    [ "PhoneShop.Logic.ILogic< CUSTOMER >", "interface_phone_shop_1_1_logic_1_1_i_logic.html", [
      [ "PhoneShop.Logic.ICustomerLogic", "interface_phone_shop_1_1_logic_1_1_i_customer_logic.html", [
        [ "PhoneShop.Logic.CustomerLogic", "class_phone_shop_1_1_logic_1_1_customer_logic.html", null ]
      ] ]
    ] ],
    [ "PhoneShop.Logic.ILogic< PHONE >", "interface_phone_shop_1_1_logic_1_1_i_logic.html", [
      [ "PhoneShop.Logic.IPhoneLogic", "interface_phone_shop_1_1_logic_1_1_i_phone_logic.html", [
        [ "PhoneShop.Logic.PhoneLogic", "class_phone_shop_1_1_logic_1_1_phone_logic.html", null ]
      ] ]
    ] ],
    [ "PhoneShop.Logic.ILogic< WORK >", "interface_phone_shop_1_1_logic_1_1_i_logic.html", [
      [ "PhoneShop.Logic.IWorkLogic", "interface_phone_shop_1_1_logic_1_1_i_work_logic.html", [
        [ "PhoneShop.Logic.WorkLogic", "class_phone_shop_1_1_logic_1_1_work_logic.html", null ]
      ] ]
    ] ],
    [ "PhoneShop.Repository.IRepository< T >", "interface_phone_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "PhoneShop.Repository.IRepository< CUSTOMER >", "interface_phone_shop_1_1_repository_1_1_i_repository.html", [
      [ "PhoneShop.Repository.ICustomerRepository", "interface_phone_shop_1_1_repository_1_1_i_customer_repository.html", [
        [ "PhoneShop.Repository.CustomerRepository", "class_phone_shop_1_1_repository_1_1_customer_repository.html", null ]
      ] ]
    ] ],
    [ "PhoneShop.Repository.IRepository< PHONE >", "interface_phone_shop_1_1_repository_1_1_i_repository.html", [
      [ "PhoneShop.Repository.IPhoneRepository", "interface_phone_shop_1_1_repository_1_1_i_phone_repository.html", [
        [ "PhoneShop.Repository.PhoneRepository", "class_phone_shop_1_1_repository_1_1_phone_repository.html", null ]
      ] ]
    ] ],
    [ "PhoneShop.Repository.IRepository< WORK >", "interface_phone_shop_1_1_repository_1_1_i_repository.html", [
      [ "PhoneShop.Repository.IWorkRepository", "interface_phone_shop_1_1_repository_1_1_i_work_repository.html", [
        [ "PhoneShop.Repository.WorkRepository", "class_phone_shop_1_1_repository_1_1_work_repository.html", null ]
      ] ]
    ] ],
    [ "PhoneShop.Logic.Test.LogicTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html", null ],
    [ "PhoneShop.Data.PHONE", "class_phone_shop_1_1_data_1_1_p_h_o_n_e.html", null ],
    [ "PhoneShop.Program.Program", "class_phone_shop_1_1_program_1_1_program.html", null ],
    [ "PhoneShop.Data.WORK", "class_phone_shop_1_1_data_1_1_w_o_r_k.html", null ]
];