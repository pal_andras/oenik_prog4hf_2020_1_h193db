var class_phone_shop_1_1_repository_1_1_phone_repository =
[
    [ "PhoneRepository", "class_phone_shop_1_1_repository_1_1_phone_repository.html#a5c868adeffb7ddc27d06d0fa58660142", null ],
    [ "CreatePhone", "class_phone_shop_1_1_repository_1_1_phone_repository.html#a11eba25b22de04662a93b527f52b64a0", null ],
    [ "DeletePhone", "class_phone_shop_1_1_repository_1_1_phone_repository.html#a34f37a394fdd8377bce614a5506bbdf0", null ],
    [ "GetAll", "class_phone_shop_1_1_repository_1_1_phone_repository.html#a6d14594e7fb64f70a689bd98801c2142", null ],
    [ "GetOne", "class_phone_shop_1_1_repository_1_1_phone_repository.html#a9dd3cc9a8431c917b2a4f460471c9b50", null ],
    [ "UpdatePhonePrice", "class_phone_shop_1_1_repository_1_1_phone_repository.html#aa0febdd9911de4a62c6ccef22488111f", null ]
];