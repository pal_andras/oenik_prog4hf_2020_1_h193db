var namespace_phone_shop_1_1_logic =
[
    [ "Test", "namespace_phone_shop_1_1_logic_1_1_test.html", "namespace_phone_shop_1_1_logic_1_1_test" ],
    [ "CustomerLogic", "class_phone_shop_1_1_logic_1_1_customer_logic.html", "class_phone_shop_1_1_logic_1_1_customer_logic" ],
    [ "ICustomerLogic", "interface_phone_shop_1_1_logic_1_1_i_customer_logic.html", "interface_phone_shop_1_1_logic_1_1_i_customer_logic" ],
    [ "ILogic", "interface_phone_shop_1_1_logic_1_1_i_logic.html", "interface_phone_shop_1_1_logic_1_1_i_logic" ],
    [ "IPhoneLogic", "interface_phone_shop_1_1_logic_1_1_i_phone_logic.html", "interface_phone_shop_1_1_logic_1_1_i_phone_logic" ],
    [ "IWorkLogic", "interface_phone_shop_1_1_logic_1_1_i_work_logic.html", "interface_phone_shop_1_1_logic_1_1_i_work_logic" ],
    [ "PhoneLogic", "class_phone_shop_1_1_logic_1_1_phone_logic.html", "class_phone_shop_1_1_logic_1_1_phone_logic" ],
    [ "WorkLogic", "class_phone_shop_1_1_logic_1_1_work_logic.html", "class_phone_shop_1_1_logic_1_1_work_logic" ]
];