var namespace_phone_shop_1_1_repository =
[
    [ "CustomerRepository", "class_phone_shop_1_1_repository_1_1_customer_repository.html", "class_phone_shop_1_1_repository_1_1_customer_repository" ],
    [ "ICustomerRepository", "interface_phone_shop_1_1_repository_1_1_i_customer_repository.html", "interface_phone_shop_1_1_repository_1_1_i_customer_repository" ],
    [ "IPhoneRepository", "interface_phone_shop_1_1_repository_1_1_i_phone_repository.html", "interface_phone_shop_1_1_repository_1_1_i_phone_repository" ],
    [ "IRepository", "interface_phone_shop_1_1_repository_1_1_i_repository.html", "interface_phone_shop_1_1_repository_1_1_i_repository" ],
    [ "IWorkRepository", "interface_phone_shop_1_1_repository_1_1_i_work_repository.html", "interface_phone_shop_1_1_repository_1_1_i_work_repository" ],
    [ "PhoneRepository", "class_phone_shop_1_1_repository_1_1_phone_repository.html", "class_phone_shop_1_1_repository_1_1_phone_repository" ],
    [ "WorkRepository", "class_phone_shop_1_1_repository_1_1_work_repository.html", "class_phone_shop_1_1_repository_1_1_work_repository" ]
];