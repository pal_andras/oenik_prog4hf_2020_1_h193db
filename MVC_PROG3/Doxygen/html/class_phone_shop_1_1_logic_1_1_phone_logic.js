var class_phone_shop_1_1_logic_1_1_phone_logic =
[
    [ "PhoneLogic", "class_phone_shop_1_1_logic_1_1_phone_logic.html#ab3b1eafd1e6da2872ff44c84f422e9c4", null ],
    [ "PhoneLogic", "class_phone_shop_1_1_logic_1_1_phone_logic.html#ab56e5db6d1eab5718fd8793cdf71c61d", null ],
    [ "AverageBrandCost", "class_phone_shop_1_1_logic_1_1_phone_logic.html#a1e6f01783f4fa4401c202bab799b0216", null ],
    [ "CreateItem", "class_phone_shop_1_1_logic_1_1_phone_logic.html#addd86611624b5395795fdd30223569bf", null ],
    [ "DeleteItem", "class_phone_shop_1_1_logic_1_1_phone_logic.html#acc278dc4a432014b4261ddc7aaa4f471", null ],
    [ "Getall", "class_phone_shop_1_1_logic_1_1_phone_logic.html#a74cce2b8d940e6d32dea51ac8b2fa233", null ],
    [ "GetMostExpensivePhone", "class_phone_shop_1_1_logic_1_1_phone_logic.html#a0abe21771011dcebbee02ee39ca62ad8", null ],
    [ "GetOne", "class_phone_shop_1_1_logic_1_1_phone_logic.html#adc7134ab62527b3a697ff0dacdf46956", null ],
    [ "UpdatePrice", "class_phone_shop_1_1_logic_1_1_phone_logic.html#ac8831d9bcd6e52d493c66e9ea7739c79", null ]
];