var class_phone_shop_1_1_logic_1_1_work_logic =
[
    [ "WorkLogic", "class_phone_shop_1_1_logic_1_1_work_logic.html#a88d4cffd111675ebedae125a4a93eea8", null ],
    [ "WorkLogic", "class_phone_shop_1_1_logic_1_1_work_logic.html#a88cfcf1ecd88149d1b9b1c1b2c26805c", null ],
    [ "CreateItem", "class_phone_shop_1_1_logic_1_1_work_logic.html#a5908f7ce27c6953dcabc373fd6ba2c17", null ],
    [ "DeleteItem", "class_phone_shop_1_1_logic_1_1_work_logic.html#af199107dcf18b90c5ea04ffa5a2ecb9a", null ],
    [ "Getall", "class_phone_shop_1_1_logic_1_1_work_logic.html#a88575baccaceda58baa28540b4bd2840", null ],
    [ "GetOne", "class_phone_shop_1_1_logic_1_1_work_logic.html#aa728bba458a70abf5c9dab974d381f61", null ],
    [ "GetTheGivenJobb", "class_phone_shop_1_1_logic_1_1_work_logic.html#ad8c42eaace8d3f3068eb883992b44584", null ],
    [ "IncreaseCustomerpoints", "class_phone_shop_1_1_logic_1_1_work_logic.html#a5e201aec2990dc4fe56469ab01227267", null ],
    [ "ListAllSevices", "class_phone_shop_1_1_logic_1_1_work_logic.html#ab90a04f2cc026ba4a3d3fec919c14512", null ],
    [ "UpdatePrice", "class_phone_shop_1_1_logic_1_1_work_logic.html#a5e04b9ba99db183baa63944ac4dc1824", null ]
];