var class_phone_shop_1_1_logic_1_1_customer_logic =
[
    [ "CustomerLogic", "class_phone_shop_1_1_logic_1_1_customer_logic.html#a476ce10fb6ab14cf1a665a0d5b0c6fc5", null ],
    [ "CustomerLogic", "class_phone_shop_1_1_logic_1_1_customer_logic.html#aff568dc8c1dba863d61fdac199ee7a3c", null ],
    [ "CreateItem", "class_phone_shop_1_1_logic_1_1_customer_logic.html#a96617cf790aa99dad04830661b07de4e", null ],
    [ "DeleteItem", "class_phone_shop_1_1_logic_1_1_customer_logic.html#a55cf0bf56f0fbff070e9b91eace8f9e1", null ],
    [ "Getall", "class_phone_shop_1_1_logic_1_1_customer_logic.html#a4bba590e8a9a443db052e8943b2a20a9", null ],
    [ "GetAllCustomerWithHighPoints", "class_phone_shop_1_1_logic_1_1_customer_logic.html#af9d15c480e2b9d308488bbf794805282", null ],
    [ "GetOne", "class_phone_shop_1_1_logic_1_1_customer_logic.html#a0b6b27f098b17d0898f5eb14dbe7b403", null ],
    [ "UpdateEmail", "class_phone_shop_1_1_logic_1_1_customer_logic.html#a02138d73c9ee2d663bc8fd1fe969c57d", null ]
];