var class_phone_shop_1_1_repository_1_1_customer_repository =
[
    [ "CustomerRepository", "class_phone_shop_1_1_repository_1_1_customer_repository.html#a261799bd9795dc7c782def806f43b0b1", null ],
    [ "CreateCustomer", "class_phone_shop_1_1_repository_1_1_customer_repository.html#ad113d6d373f70060389c6e3f9a68ad6a", null ],
    [ "DeleteCustomer", "class_phone_shop_1_1_repository_1_1_customer_repository.html#a20263ef6c10cffb1ee3d829f411ebc45", null ],
    [ "GetAll", "class_phone_shop_1_1_repository_1_1_customer_repository.html#ad2032d25b36f5616d089a0601f6671ec", null ],
    [ "GetOne", "class_phone_shop_1_1_repository_1_1_customer_repository.html#add838c326e1054fd60ffb6de7c02cdec", null ],
    [ "UpdateCustomerEmail", "class_phone_shop_1_1_repository_1_1_customer_repository.html#a07465eaa00de6da2f6b448dd6d282559", null ]
];