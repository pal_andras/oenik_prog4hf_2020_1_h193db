var searchData=
[
  ['customer_181',['CUSTOMER',['../class_phone_shop_1_1_data_1_1_phone_shop_data_base_entities.html#a59a70256dd543219924ea44556266c5f',1,'PhoneShop.Data.PhoneShopDataBaseEntities.CUSTOMER()'],['../class_phone_shop_1_1_data_1_1_w_o_r_k.html#a300d87fb7c1b8db7c8132eb1a3f7087a',1,'PhoneShop.Data.WORK.CUSTOMER()']]],
  ['customer_5fdate_182',['customer_date',['../class_phone_shop_1_1_data_1_1_c_u_s_t_o_m_e_r.html#ad57ddabc8bce0c165d35349ab28a20de',1,'PhoneShop::Data::CUSTOMER']]],
  ['customer_5femail_183',['customer_email',['../class_phone_shop_1_1_data_1_1_c_u_s_t_o_m_e_r.html#ace2c0c2227ec42cb8e8bf4309f5871ad',1,'PhoneShop::Data::CUSTOMER']]],
  ['customer_5fid_184',['customer_id',['../class_phone_shop_1_1_data_1_1_c_u_s_t_o_m_e_r.html#a45d2c3dec4146a9b75dc48760b7e8ac0',1,'PhoneShop::Data::CUSTOMER']]],
  ['customer_5fname_185',['customer_name',['../class_phone_shop_1_1_data_1_1_c_u_s_t_o_m_e_r.html#a96be98687a28c5bf5cc0182e1dc882bc',1,'PhoneShop::Data::CUSTOMER']]],
  ['customer_5fphonenumber_186',['customer_phonenumber',['../class_phone_shop_1_1_data_1_1_c_u_s_t_o_m_e_r.html#a506f0a4a2325741ad48d265236d136f1',1,'PhoneShop::Data::CUSTOMER']]],
  ['customer_5fpoints_187',['customer_points',['../class_phone_shop_1_1_data_1_1_c_u_s_t_o_m_e_r.html#a85ab3d89a74cf520f4b7bc7a6867fbf8',1,'PhoneShop::Data::CUSTOMER']]],
  ['customer_5fwork_188',['customer_work',['../class_phone_shop_1_1_data_1_1_c_u_s_t_o_m_e_r.html#a33b87bc9dc75982a986bac7b8e97bbc3',1,'PhoneShop::Data::CUSTOMER']]]
];
