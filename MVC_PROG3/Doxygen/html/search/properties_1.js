var searchData=
[
  ['phone_189',['PHONE',['../class_phone_shop_1_1_data_1_1_phone_shop_data_base_entities.html#a83a7c8813ef59eed18a3faf309f99fdd',1,'PhoneShop.Data.PhoneShopDataBaseEntities.PHONE()'],['../class_phone_shop_1_1_data_1_1_w_o_r_k.html#a50f8c837c492233e71cbb577f274fed4',1,'PhoneShop.Data.WORK.PHONE()']]],
  ['phone_5fbrand_190',['phone_brand',['../class_phone_shop_1_1_data_1_1_p_h_o_n_e.html#a86c0319835a19d2436b52a92c7fb79e4',1,'PhoneShop::Data::PHONE']]],
  ['phone_5fid_191',['phone_id',['../class_phone_shop_1_1_data_1_1_p_h_o_n_e.html#a1fef5151f3429ef309e6cae0bf4396d3',1,'PhoneShop::Data::PHONE']]],
  ['phone_5finch_192',['phone_inch',['../class_phone_shop_1_1_data_1_1_p_h_o_n_e.html#a2842775d358039357e0b6e68851429cb',1,'PhoneShop::Data::PHONE']]],
  ['phone_5finstock_193',['phone_instock',['../class_phone_shop_1_1_data_1_1_p_h_o_n_e.html#ae16618e5358c7241573c5fe0323f603f',1,'PhoneShop::Data::PHONE']]],
  ['phone_5fprice_194',['phone_price',['../class_phone_shop_1_1_data_1_1_p_h_o_n_e.html#ae64e26fff929501dcb3d8786aea18ecd',1,'PhoneShop::Data::PHONE']]],
  ['phone_5ftype_195',['phone_type',['../class_phone_shop_1_1_data_1_1_p_h_o_n_e.html#a5778bdcca2a3d0d72f4f758576009991',1,'PhoneShop::Data::PHONE']]]
];
