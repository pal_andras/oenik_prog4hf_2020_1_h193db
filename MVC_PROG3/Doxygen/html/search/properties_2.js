var searchData=
[
  ['work_196',['WORK',['../class_phone_shop_1_1_data_1_1_c_u_s_t_o_m_e_r.html#aa4aa7bb62d0e4b4dc64505e9e3fe588b',1,'PhoneShop.Data.CUSTOMER.WORK()'],['../class_phone_shop_1_1_data_1_1_p_h_o_n_e.html#aee4582a2d019e5dbf37f1e3074292925',1,'PhoneShop.Data.PHONE.WORK()'],['../class_phone_shop_1_1_data_1_1_phone_shop_data_base_entities.html#a1ce28d95dd106e1fad7efa495bb3612f',1,'PhoneShop.Data.PhoneShopDataBaseEntities.WORK()']]],
  ['work_5fid_197',['work_id',['../class_phone_shop_1_1_data_1_1_w_o_r_k.html#aa8427e6dfdd7ee8bfb09019cf77f6062',1,'PhoneShop::Data::WORK']]],
  ['work_5fname_198',['work_name',['../class_phone_shop_1_1_data_1_1_w_o_r_k.html#a1f080f90a4db76bc60c11d82f4100271',1,'PhoneShop::Data::WORK']]],
  ['work_5fparts_199',['work_parts',['../class_phone_shop_1_1_data_1_1_w_o_r_k.html#a3f55ff76fb14d4538dac89f163d3ecc6',1,'PhoneShop::Data::WORK']]],
  ['work_5fphone_200',['work_phone',['../class_phone_shop_1_1_data_1_1_w_o_r_k.html#af433d970b103c33e36b6268e3f0a3e40',1,'PhoneShop::Data::WORK']]],
  ['work_5fprice_201',['work_price',['../class_phone_shop_1_1_data_1_1_w_o_r_k.html#afa4093998aac7b9ea9dbedb6853dedb4',1,'PhoneShop::Data::WORK']]],
  ['work_5ftime_202',['work_time',['../class_phone_shop_1_1_data_1_1_w_o_r_k.html#ac7299996a8bfc063e7a92b6f0851bf45',1,'PhoneShop::Data::WORK']]],
  ['work_5fworker_203',['work_worker',['../class_phone_shop_1_1_data_1_1_w_o_r_k.html#a15209562138b80c96a438447b97aaba4',1,'PhoneShop::Data::WORK']]]
];
