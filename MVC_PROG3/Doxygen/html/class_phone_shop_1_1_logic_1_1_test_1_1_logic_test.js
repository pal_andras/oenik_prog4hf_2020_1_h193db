var class_phone_shop_1_1_logic_1_1_test_1_1_logic_test =
[
    [ "AddCustomerTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a4b9e1415118d5f45855e97b1648f860a", null ],
    [ "AddPhoneTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#afed4579d1aa229961096be1895eb2cd8", null ],
    [ "AddWorkTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a9ee13c146f215868923600220713a589", null ],
    [ "AverageBrandCostTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a7526dfff4fe4c98792a81395e6df5bf9", null ],
    [ "DeleteCustomerTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a2991f93489c8c397f389319c72ece74b", null ],
    [ "DeletePhoneTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a11dbc8dc2b44b21f84c3212e9704f900", null ],
    [ "DeleteWorkTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#ae661a40de193b60ee4360ec9c590b152", null ],
    [ "GetAllcustomerTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a6ae983da133cdbb36882bbd8ed42202d", null ],
    [ "GetAllPhoneTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a847b7d25c941d5a04dcf52f4824ba731", null ],
    [ "GetAllWorkTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a0a3f5261f80c5c471ac526f351f9dda9", null ],
    [ "GetOneCustomerTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#ac41d1873bb69710e805670245936d5f7", null ],
    [ "GetOnePhoneTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a0d8d9335a0251f54c847eb25e964876d", null ],
    [ "GetOneWorkTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a661d980987e508892edfb8c4455133f6", null ],
    [ "GetTheGivenJobbTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a5f80b0a9989e8cd306868874bff34522", null ],
    [ "IncreaseCustomerpointsTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a8c41b6b21c906a893883df8819df5ee5", null ],
    [ "Init", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#af13b3a9f795bd1264844cc0461f0800f", null ],
    [ "ListAllServicestest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a73951871fa3f01309a319929182d1a32", null ],
    [ "MostExpensivePhoneTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a4ce5c352cde3da4109dc3a0481e5eac7", null ],
    [ "UpdateCustomerEmailTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a47790bbb6179dba12b29877545d1402a", null ],
    [ "UpdatePhonePriceTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#aea494fe55dc13532d7c7ce0afc05c098", null ],
    [ "UpdateWorkPriceTest", "class_phone_shop_1_1_logic_1_1_test_1_1_logic_test.html#a6a257eff37aeebe4933f56094378e1bc", null ]
];