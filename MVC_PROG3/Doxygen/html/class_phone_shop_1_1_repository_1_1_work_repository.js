var class_phone_shop_1_1_repository_1_1_work_repository =
[
    [ "WorkRepository", "class_phone_shop_1_1_repository_1_1_work_repository.html#a7cfcb5d96140fbfe94aef46338951a67", null ],
    [ "CreateWork", "class_phone_shop_1_1_repository_1_1_work_repository.html#ac1bdcaf22ed665e5ffc3d87b31c4377d", null ],
    [ "DeleteWork", "class_phone_shop_1_1_repository_1_1_work_repository.html#a72359870bc042e0273c20bda65d2d84d", null ],
    [ "GetAll", "class_phone_shop_1_1_repository_1_1_work_repository.html#a2c28d7299a85def3b40692d5d48eb133", null ],
    [ "GetOne", "class_phone_shop_1_1_repository_1_1_work_repository.html#af060b9e061a9c8205711ae239f5537dd", null ],
    [ "UpdateWorkPrice", "class_phone_shop_1_1_repository_1_1_work_repository.html#a68629c3d31791b047d5cb9b16bff7442", null ]
];