﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PhoneShop.ConsoleClient
{
	public class Customer
	{
		public int Id { get; set; }
		public int WorkId { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string Date { get; set; }
		public int CustomerPoints { get; set; }
		public override string ToString()
		{
			return $"Id={Id}\tName={Name}\tEmail={Email}\tPhoneNumber={PhoneNumber}\tDate={Date}\tCustomerPoints={CustomerPoints}\tWorkId={WorkId}";
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			Random r = new Random();
			string id = r.Next(1, 100).ToString();
			Console.WriteLine("LOADING....");
			Console.ReadLine();
			string url = "http://localhost:58665/api/CustomerApi/";
			using(HttpClient client = new HttpClient())
			{
				string json = client.GetStringAsync(url + "all").Result;
				var list = JsonConvert.DeserializeObject<List<Customer>>(json);
				foreach (var item in list)
				{
					Console.WriteLine(item);
				}
				Console.ReadLine();
				Dictionary<string, string> postData;
				string response;
				postData = new Dictionary<string, string>();
				postData.Add(nameof(Customer.Id), id);
				postData.Add(nameof(Customer.Name), "Pál András");
				postData.Add(nameof(Customer.Email), "pal.andras55@gmail.com");
				postData.Add(nameof(Customer.PhoneNumber), "06208086841");
				postData.Add(nameof(Customer.Date), "2020.05.01.");
				postData.Add(nameof(Customer.CustomerPoints), "100");
				postData.Add(nameof(Customer.WorkId), "1");
				response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
				json = client.GetStringAsync(url + "all").Result;
				Console.WriteLine("ADD: " + response);
				Console.WriteLine("ALL: " + json);
				Console.ReadLine();

				int carId = JsonConvert.DeserializeObject<List<Customer>>(json).Single(x => x.Name == "Pál András").Id;
				postData = new Dictionary<string, string>();
				postData.Add(nameof(Customer.Id), carId.ToString());
				postData.Add(nameof(Customer.Name), "Pál András");
				postData.Add(nameof(Customer.Email), "Pál Andráspal.andras55@gmail.com");
				postData.Add(nameof(Customer.PhoneNumber), "06208086841");
				postData.Add(nameof(Customer.Date), "2019.05.01.");
				postData.Add(nameof(Customer.CustomerPoints), "1000");
				postData.Add(nameof(Customer.WorkId), "1");
				response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
				json = client.GetStringAsync(url + "all").Result;
				Console.WriteLine("MOD: " + response);
				Console.WriteLine("ALL: " + json);
				Console.ReadLine();

				response = client.GetStringAsync(url + "del/" + carId).Result;
				json = client.GetStringAsync(url + "all").Result;
				Console.WriteLine("DEL: " + response);
				Console.WriteLine("ALL: " + json);
				Console.ReadLine();
			}
		}
	}
}
