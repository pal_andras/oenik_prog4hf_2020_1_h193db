CREATE TABLE PHONE
(
	phone_id int primary key,
	phone_brand nvarchar(100),
	phone_type nvarchar(100),
	phone_price int,
	phone_inch float,
	phone_instock int
);

CREATE TABLE WORK
(
	work_id int primary key, 
	work_phone int references PHONE(phone_id),
	work_name nvarchar(100),
	work_price int,
	work_time int,
	work_parts nvarchar(100),
	work_worker nvarchar(100)
);

CREATE TABLE CUSTOMER
(
	customer_id int IDENTITY primary key,
	customer_work int references WORK(work_id), 
	customer_name nvarchar(100),
	customer_email nvarchar(100),
	customer_phonenumber nvarchar(100),
	customer_date nvarchar(100),
	customer_points int
);

INSERT INTO PHONE VAlUES (1,'Iphone','11',280000, 6.1 , 1);
INSERT INTO PHONE VAlUES (2,'Iphone','10',230000, 5.85 , 1);
INSERT INTO PHONE VAlUES (3,'Iphone','8',175000, 5.5 , 1);
INSERT INTO PHONE VAlUES (4,'Iphone','7',120000, 4.7 , 0);
INSERT INTO PHONE VAlUES (5,'Iphone','11 pro max',400000, 6.5 , 0);
INSERT INTO PHONE VAlUES (6,'Samsung','s10',200000, 6.1 , 1);
INSERT INTO PHONE VAlUES (7,'Samsung','s9',170000, 5.8 , 1);
INSERT INTO PHONE VAlUES (8,'Samsung','s8',130000, 5.8 , 0);
INSERT INTO PHONE VAlUES (9,'Samsung','s7',98000, 5.1 , 1);
INSERT INTO PHONE VAlUES (10,'Huawei','p20 pro',130000, 5.8 , 1);

INSERT INTO WORK VAlUES (1, 1,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (2, 2,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (3, 3,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (4, 4,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (5, 5,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (6, 6,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (7, 7,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (8, 8,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (9, 9,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (10, 10,'Cleaning', 5000, 30 , 'No', 'Kovács Béla');
INSERT INTO WORK VAlUES (11, 1,'Camera fix', 20000, 90 , 'Yes', 'Nagy János');
INSERT INTO WORK VAlUES (12, 2,'Camera fix', 17000, 90 , 'Yes', 'Nagy János');
INSERT INTO WORK VAlUES (13, 3,'Camera fix', 15000, 90 , 'Yes', 'Nagy János');
INSERT INTO WORK VAlUES (14, 4,'Camera fix', 10000, 90 , 'Yes', 'Nagy János');
INSERT INTO WORK VAlUES (15, 5,'Camera fix', 22000, 90 , 'Yes', 'Nagy János');
INSERT INTO WORK VAlUES (16, 1,'Screen replacement', 80000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (17, 2,'Screen replacement', 70000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (18, 3,'Screen replacement', 60000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (19, 4,'Screen replacement', 50000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (20, 5,'Screen replacement', 90000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (21, 6,'Screen replacement', 70000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (22, 7,'Screen replacement', 60000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (23, 8,'Screen replacement', 50000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (24, 9,'Screen replacement', 40000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (25, 10,'Screen replacement', 50000, 15 , 'Yes', 'Kovács Béla');
INSERT INTO WORK VAlUES (26, 6,'Battery change', 10000, 45 , 'Yes', 'Nagy János');
INSERT INTO WORK VAlUES (27, 7,'Battery change', 10000, 45 , 'Yes', 'Nagy János');
INSERT INTO WORK VAlUES (28, 8,'Battery change', 10000, 45 , 'Yes', 'Nagy János');
INSERT INTO WORK VAlUES (29, 9,'Battery change', 10000, 45 , 'Yes', 'Nagy János');
INSERT INTO WORK VAlUES (30, 1,'Software fix', 8000, 60 , 'No', 'Nagy János');
INSERT INTO WORK VAlUES (31, 2,'Software fix', 8000, 60 , 'No', 'Nagy János');
INSERT INTO WORK VAlUES (32, 3,'Software fix', 8000, 60 , 'No', 'Nagy János');
INSERT INTO WORK VAlUES (33, 4,'Software fix', 8000, 60 , 'No', 'Nagy János');
INSERT INTO WORK VAlUES (34, 5,'Software fix', 8000, 60 , 'No', 'Nagy János');
INSERT INTO WORK VAlUES (35, 6,'Software fix', 8000, 60 , 'No', 'Nagy János');
INSERT INTO WORK VAlUES (36, 7,'Software fix', 8000, 60 , 'No', 'Nagy János');
INSERT INTO WORK VAlUES (37, 8,'Software fix', 8000, 60 , 'No', 'Nagy János');
INSERT INTO WORK VAlUES (38, 9,'Software fix', 8000, 60 , 'No', 'Nagy János');
INSERT INTO WORK VAlUES (39, 10,'Software fix', 8000, 60 , 'No', 'Nagy János');

INSERT INTO CUSTOMER VAlUES (1, 1,'Pál András', 'pal.andras55@gmail.com', 06208086841 , '2020.01.01.', 100);
INSERT INTO CUSTOMER VAlUES (2, 2,'Kis Ágnes', 'acadipe-8801@yopmail.com', 06208586841 , '2019.01.06.', 10);
INSERT INTO CUSTOMER VAlUES (3, 3,'Nagy Bálint', 'iffottabe-8893@yopmail.com', 06608086841 , '2019.01.08.', 200);
INSERT INTO CUSTOMER VAlUES (4, 4,'Karlik Fanni', 'xupummova-1275@yopmail.com', 06278086841 , '2019.01.13.', 20);
INSERT INTO CUSTOMER VAlUES (5, 5,'Gálcsik Réka', 'amecedec-7895@yopmail.com', 06208886841 , '2019.01.24.', 50);
INSERT INTO CUSTOMER VAlUES (6, 6,'Hollosy Boglárka', 'xydoppihaxe-4309@yopmail.com', 06209386841 , '20219.08.01.', 300);
INSERT INTO CUSTOMER VAlUES (7, 7,'Sztáray Andrea', 'ellajesef-9401@yopmail.com', 06208086844 , '2019.01.15.', 60);
INSERT INTO CUSTOMER VAlUES (8, 8,'Horvát Attila', 'kixequcy-7976@yopmail.com', 06208088465 , '2019.08.01.', 50);
INSERT INTO CUSTOMER VAlUES (9, 10,'Német Viktor', 'annorannuz-0729@yopmail.com', 06208086893 , '2019.12.05.', 40);
INSERT INTO CUSTOMER VAlUES (10, 11,'Orbán János', 'ellovozu-6399@yopmail.com', 06208085847 , '2019.11.09.', 20);
INSERT INTO CUSTOMER VAlUES (11, 15,'Varga Sándor', 'eqewocer-3660@yopmail.com', 06208084840 , '2019.06.03.', 10);
INSERT INTO CUSTOMER VAlUES (12, 29,'Szabó Benedek', 'akossuly-9003@yopmail.com', 06208086841 , '2019.02.02.', 170);
INSERT INTO CUSTOMER VAlUES (13, 21,'Balogh Gábor', 'norewegaz-9344@yopmail.com', 06208626841 , '2019.06.09.', 70);
INSERT INTO CUSTOMER VAlUES (14, 18,'Cigány Bence', 'reqoqekela-6978@yopmail.com', 06708086842 , '2018.05.30.', 80);
INSERT INTO CUSTOMER VAlUES (15, 16,'Marton István', 'wyvejexi-2735@yopmail.com', 06208086865 , '2018.04.09.', 180);
INSERT INTO CUSTOMER VAlUES (16, 9,'Herczeg Gergő', 'ennenygul-1712@yopmail.com', 06208086899 , '2018.07.01.', 90);
INSERT INTO CUSTOMER VAlUES (17, 31,'Hajzer Enikő', 'ogyssefib-2931@yopmail.com', 06208036865 , '2017.07.05.', 190);
INSERT INTO CUSTOMER VAlUES (18, 24,'Pongrác Balázs', 'hallemuvydd-0910@yopmail.com', 063051638 , '2018.12.05.', 250);
INSERT INTO CUSTOMER VAlUES (19, 23,'Salai Angéla', 'jilerava-1736@yopmail.com', 06208086765 , '2019.12.22.', 110);
INSERT INTO CUSTOMER VAlUES (20, 13,'Márton Csilla', 'ajixoduw-6973@yopmail.com', 06208086432 , '2018.04.28.', 250);
INSERT INTO CUSTOMER VAlUES (21, 9,'Králik József', 'vanyddyssape-1319@yopmail.com', 06208085649 , '2018.01.26.', 120);