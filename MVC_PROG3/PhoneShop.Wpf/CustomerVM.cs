﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace PhoneShop.Wpf
{
	class CustomerVM :ObservableObject
	{
		private int id;
		private string name;
		private string email;
		private string phoneNumber;
		private string date;
		private int points;
		private int workId;
		public int Id
		{
			get { return id; }
			set { Set(ref id, value); }
		}
		public string Name
		{
			get { return name; }
			set { Set(ref name, value); }
		}
		public string Email
		{
			get { return email; }
			set { Set(ref email, value); }
		}
		public string PhoneNumber
		{
			get { return phoneNumber; }
			set { Set(ref phoneNumber, value); }
		}
		public string Date
		{
			get { return date; }
			set { Set(ref date, value); }
		}
		public int Points
		{
			get { return points; }
			set { Set(ref points, value); }
		}
		public int WorkId
		{
			get { return workId; }
			set { Set(ref workId, value); }
		}
		public void CopyFrom(CustomerVM otherCustomer)
		{
			if (otherCustomer == null)
			{
				return;
			}
			this.Id = otherCustomer.Id;
			this.Name = otherCustomer.Name;
			this.Email = otherCustomer.Email;
			this.PhoneNumber = otherCustomer.PhoneNumber;
			this.Date = otherCustomer.Date;
			this.Points = otherCustomer.Points;
			this.WorkId = otherCustomer.WorkId;
		}
	}
}
